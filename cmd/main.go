package main

import (
	"context"
	"flag"
	"github.com/jackc/pgx/v4/pgxpool"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/timestamppb"
	"log"
	"net"
	"time"
	sq "github.com/Masterminds/squirrel"

	"auth/internal/config"
	desc "auth/pkg/user_v1"
)

var configPath string

func init() {
	flag.StringVar(&configPath, "config-path", ".env", "path to config file")
}

type server struct {
	desc.UnimplementedUserAPIServer
	pool *pgxpool.Pool
}

// TODO: Add return error and log info about error

func (s *server) Delete(ctx context.Context, req *desc.DeleteRequest) (*emptypb.Empty, error) {
	log.Println(req.GetId());
	sql, args, err  := sq.Update("users").Set("is_delete", true).Where(sq.Eq{"id" : req.GetId()}).PlaceholderFormat(sq.Dollar).ToSql()

	if err != nil {
		log.Println("Error")
		return &emptypb.Empty{}, nil
	}
    if err != nil {
		log.Println("Error")
        return &emptypb.Empty{}, nil
    }
    log.Printf("Delete user with id %d", req.GetId())
    s.pool.Exec(context.Background(), sql, args...)
	return &emptypb.Empty{}, nil
}

func (s *server) Update(ctx context.Context, req *desc.UpdateRequest) (*emptypb.Empty, error) {
	if req.Info.Email == nil && req.Info.Name == nil {
		log.Printf("Not value to update")
		return &emptypb.Empty{}, nil		
	}

	query := sq.Update("users")
	if req.Info.Email != nil {
		query = query.Set("email", req.Info.Email.Value)
	}

	if req.Info.Name != nil {
		query = query.Set("name", req.Info.Name.Value)
	}

	sql, args, err  := query.Where(sq.Eq{"id" : req.GetId()}).PlaceholderFormat(sq.Dollar).ToSql()


	if err != nil {
		log.Println("Error")
		return &emptypb.Empty{}, nil
	}
    if err != nil {
		log.Println("Error")
        return &emptypb.Empty{}, nil
    }
    log.Printf("Update user with id %d", req.GetId())
    s.pool.Exec(context.Background(), sql, args...)
	return &emptypb.Empty{}, nil
}

func (s *server) Get(ctx context.Context, req *desc.GetRequest) (*desc.GetResponse, error) {
	builderSelectOne := sq.Select("id", "name", "email", "password", "updated_at", "created_at").
		From("users").
		PlaceholderFormat(sq.Dollar).
		Where(sq.Eq{"id": req.GetId()}).
		Limit(1)

	query, args, err := builderSelectOne.ToSql()
	if err != nil {
		log.Fatalf("failed to build query: %v", err)
	}

	var id int
	var name, email, password string
	var role desc.Role
	var createdAt time.Time
	var updatedAt time.Time

	err = s.pool.QueryRow(ctx, query, args...).Scan(&id, &name, &email, &password, &updatedAt, &createdAt)
	if err != nil {
		log.Fatalf("failed to select notes: %v", err)
	}

	log.Printf("id: %d, name: %s, email: %s, password: %s, created_at: %v, updated_at: %v\n", id, name, email, password,  createdAt, updatedAt)

	// var updatedAtTime *timestamppb.Timestamp
	// if updatedAt.Valid {
	// 	updatedAtTime = timestamppb.New(updatedAt.Time)
	// }

	return &desc.GetResponse{
		User: &desc.User{
			Id: req.GetId(),
			Info: &desc.UserInfo{
				Name:     name,
				Email:    email,
				Password: password,
				Role:     role,
			},
			CreatedAt: timestamppb.New(createdAt),
			UpdatedAt: timestamppb.New(updatedAt),
		}}, nil
}

func (s *server) Create(ctx context.Context, req *desc.CreateRequest) (*desc.CreateResponse, error) {
		// Делаем запрос на вставку записи в таблицу note
	builderInsert := sq.Insert("users").
		PlaceholderFormat(sq.Dollar).
		Columns("name", "email", "password", "role", "updated_at").
		Values(&req.Info.Name, &req.Info.Email, &req.Info.Password, req.Info.Role.String(), time.Now()).
		Suffix("RETURNING id")

	query, args, err := builderInsert.ToSql()
	if err != nil {
		log.Fatalf("failed to build query: %v", err)
	}

	var noteID int64
	err = s.pool.QueryRow(ctx, query, args...).Scan(&noteID)
	if err != nil {
		log.Fatalf("failed to insert note: %v", err)
	}

	log.Printf("inserted note with id: %d", noteID)

	return &desc.CreateResponse{Id: noteID}, nil
}

func main() {
	flag.Parse()
	ctx := context.Background()

	// Считываем переменные окружения
	err := config.Load(configPath)
	if err != nil {
		log.Fatalf("failed to load config: %v", err)
	}

	grpcConfig, err := config.NewGRPCConfig()
	if err != nil {
		log.Fatalf("failed to get grpc config: %v", err)
	}

	pgConfig, err := config.NewPGConfig()
	if err != nil {
		log.Fatalf("failed to get pg config: %v", err)
	}

	lis, err := net.Listen("tcp", grpcConfig.Address())
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	// Создаем пул соединений с базой данных
	pool, err := pgxpool.Connect(ctx, pgConfig.DSN())
	if err != nil {
		log.Fatalf("failed to connect to database: %v", err)
	}
	defer pool.Close()

	s := grpc.NewServer()
	reflection.Register(s)
	desc.RegisterUserAPIServer(s, &server{pool: pool})

	log.Printf("server listening at %v", lis.Addr())

	if err = s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
