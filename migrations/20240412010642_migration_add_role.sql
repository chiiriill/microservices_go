-- +goose Up
-- +goose StatementBegin
SELECT 'up SQL query';

ALTER TABLE users
ADD role varchar(255);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
